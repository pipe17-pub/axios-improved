const axios = require('axios')
const retry = require('async-retry')

const wrap = (obj) => {
    let handler = {
        get(target, propKey, receiver) {
            const origMethod = target[propKey]
            return async function (...args) {

                const helper = async (bail, attempt) => {
                    return await origMethod.apply(this, args)
                }
            
                try {
                    return await retry(helper, { retries: 1 })
                }
                catch (e) {
                    delete e.request
                    if (e.response) {
                        delete e.response.request
                    }
                    throw e
                }
            }
        }
    }
    return new Proxy(obj, handler)
}

module.exports = wrap(axios)
